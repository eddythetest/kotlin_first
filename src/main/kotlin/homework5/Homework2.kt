package homework5

fun main() {
    val digit = readLine()!!.toInt()
    reverseNumber(digit)
}

fun reverseNumber(inputNumber2: Int): Int {
    var resultNumberAsString = ""
    var inputNumber = inputNumber2
    while (resultNumberAsString.length < inputNumber2.toString().length) {
        resultNumberAsString += (inputNumber % 10).toString()
        inputNumber /= 10
    }
    val resultNumberAsInt = resultNumberAsString.toInt()
    println(resultNumberAsInt)
    return resultNumberAsInt
}
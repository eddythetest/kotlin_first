package homework5

class Lion(var name: String, var height: Double, var weight: Double) {
    val foodPreference: Array<String> = arrayOf("Beef", "Chicken")
    var satiety: Int = 0

    fun eat(foodEaten: String) {
        if (foodEaten in foodPreference) {
            satiety += 1
            println("Сытость увеличилась: $satiety")
        } else {
            println("Неправильная еда!")
        }
    }
}

class Tiger(var name: String, var height: Double, var weight: Double) {
    val foodPreference: Array<String> = arrayOf("Beef", "Chicken")
    var satiety: Int = 0

    fun eat(foodEaten: String) {
        if (foodEaten in foodPreference) {
            satiety += 1
            println("Сытость увеличилась: $satiety")
        } else {
            println("Неправильная еда!")
        }
    }
}

class Hippopotamus(var name: String, var height: Double, var weight: Double) {
    val foodPreference: Array<String> = arrayOf("Watermelon", "Grass")
    var satiety: Int = 0

    fun eat(foodEaten: String) {
        if (foodEaten in foodPreference) {
            satiety += 1
            println("Сытость увеличилась: $satiety")
        } else {
            println("Неправильная еда!")
        }
    }
}

class Wolf(var name: String, var height: Double, var weight: Double) {
    val foodPreference: Array<String> = arrayOf("Beef", "Chicken")
    var satiety: Int = 0

    fun eat(foodEaten: String) {
        if (foodEaten in foodPreference) {
            satiety += 1
            println("Сытость увеличилась: $satiety")
        } else {
            println("Неправильная еда!")
        }
    }
}

class Giraffe(var name: String, var height: Double, var weight: Double) {
    val foodPreference: Array<String> = arrayOf("Leaves", "Grass")
    var satiety: Int = 0

    fun eat(foodEaten: String) {
        if (foodEaten in foodPreference) {
            satiety += 1
            println("Сытость увеличилась: $satiety")
        } else {
            println("Неправильная еда!")
        }
    }
}


class Elephant(var name: String, var height: Double, var weight: Double) {
    val foodPreference: Array<String> = arrayOf("Leaves", "Nuts")
    var satiety: Int = 0

    fun eat(foodEaten: String) {
        if (foodEaten in foodPreference) {
            satiety += 1
            println("Сытость увеличилась: $satiety")
        } else {
            println("Неправильная еда!")
        }
    }
}


class Chimpanzee(var name: String, var height: Double, var weight: Double) {
    val foodPreference: Array<String> = arrayOf("Banana", "Mango")
    var satiety: Int = 0

    fun eat(foodEaten: String) {
        if (foodEaten in foodPreference) {
            satiety += 1
            println("Сытость увеличилась: $satiety")
        } else {
            println("Неправильная еда!")
        }
    }
}


class Gorilla(var name: String, var height: Double, var weight: Double) {
    val foodPreference: Array<String> = arrayOf("Banana", "Bamboo")
    var satiety: Int = 0

    fun eat(foodEaten: String) {
        if (foodEaten in foodPreference) {
            satiety += 1
            println("Сытость увеличилась: $satiety")
        } else {
            println("Неправильная еда!")
        }
    }
}


fun main() {
    val lionMike = Lion("Mike", 12.4, 123.5)
    lionMike.eat("Chicken")
    lionMike.eat("Fish")

    val tigetJohn = Tiger("John", 12.4, 123.5)
    tigetJohn.eat("Beef")
    tigetJohn.eat("Carrots")

    val hippopotamusCarl = Hippopotamus("Carl", 12.4, 123.5)
    hippopotamusCarl.eat("Watermelon")
    hippopotamusCarl.eat("Meat")

    val wolfWolfgang = Wolf("Wolfgang", 12.4, 123.5)
    wolfWolfgang.eat("Chicken")
    wolfWolfgang.eat("Berries")

    val giraffeGarry = Giraffe("Garry", 12.4, 123.5)
    giraffeGarry.eat("Leaves")
    giraffeGarry.eat("Meat")

    val elephantElla = Elephant("Ella", 12.4, 123.5)
    elephantElla.eat("Nuts")
    elephantElla.eat("Fish")

    val chimpanzeeCharles = Chimpanzee("Charles", 12.4, 123.5)
    chimpanzeeCharles.eat("Banana")
    chimpanzeeCharles.eat("Grass")

    val gorillaGavin = Gorilla("Gavin", 12.4, 123.5)
    gorillaGavin.eat("Bamboo")
    gorillaGavin.eat("Meat")
}
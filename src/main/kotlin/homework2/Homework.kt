package homework2

fun main(){

    val whiskeyVolume: Byte = 50
    val pinaColladaVolume: Short = 200
    val lemonadeVolume: Int = 18500
    val freshVolume: Long = 3000000000L
    val colaVolume: Float = 0.5f
    val aleVolume: Double = 0.666666667
    val specialty: String = "Что-то авторское!"

    println("Заказ - '$lemonadeVolume мл лимонада' готов!")
    println("Заказ - '$pinaColladaVolume мл пина колады' готов!")
    println("Заказ - '$whiskeyVolume мл виски' готов!")
    println("Заказ - '$freshVolume капель фреша' готов!")
    println("Заказ - '$colaVolume литра колы' готов!")
    println("Заказ - '$aleVolume литра эля' готов!")
    println("Заказ - '$specialty' готов!")

}
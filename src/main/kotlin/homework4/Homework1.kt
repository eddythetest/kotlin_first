package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)

    fun countEvenAndOddNumbers(inputArray : Array<Int>){
        var amountOfOdds : Int = 0
        var amountOfEven : Int = 0
        for(i in inputArray.indices){
            if(inputArray[i]%2==0){
                amountOfEven ++
            } else {
                amountOfOdds++
            }
        }
        println("$amountOfOdds нечетных чисел в массиве")
        println("$amountOfEven четных чисел в массиве")
    }

    countEvenAndOddNumbers(myArray)
}

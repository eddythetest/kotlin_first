package homework4

fun main() {

    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)

    fun countGradesPercentage(classGrades: Array<Int>) {
        var twos: Int = 0
        var threes: Int = 0
        var fours: Int = 0
        var fives: Int = 0
        for (x in classGrades.indices) {
            when (classGrades[x]) {
                2 -> twos++
                3 -> threes++
                4 -> fours++
                5 -> fives++
            }
        }
        println("${String.format("%.2f", twos.toFloat() / classGrades.size * 100)} двоечников")
        println("${String.format("%.2f", threes.toFloat() / classGrades.size * 100)} троечников")
        println("${String.format("%.2f", fours.toFloat() / classGrades.size * 100)} хорошистов")
        println("${String.format("%.2f", fives.toFloat() / classGrades.size * 100)} отличников")
    }

    countGradesPercentage(marks)

}